const express = require("express");
// We store express module to a variable so we can easily access its keywords, functions and methods.
// This code will help us access contents of express module or package
    // A module is a software component or part of a program that contains 1 or more routines.
// It also allow us to access methods and functions that we will use to easily create an app or server.

const app = express();
// app = server 
// This code creates an application using / aka express application.

const port = 3000;
// For our application server to run, we need a port to listen to
app.use(express.json())
app.use(express.urlencoded({extended: true}));
// It allows your app to read data from forms
// By default, information received from the url can only be received as a string or an array.
// By applying the option of "extended:true", this allows us to recive information through other data types. Such as an object which we will use through out our application.

app.get("/hello", (request, response) => {
    response.send("GET method success. \nHello from /hello endpoint!")
    // This is the response that we will expect to receive if the get method with the right endpoint is successful
});
// This route expects to receive a GET request at the URL/endpoint "/hello"

app.post("/hello", (request, response) => {
    response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`)
})

let users = [];

app.post("/signup", (request, response) => {
    if(request.body.username !== "" && request.body.password !== ""){
        users.push(request.body)
        console.log(users);
        response.send(`User ${request.body.username} is successfully registered`)
    } else {
        response.send("Please input BOTH username and password");
    }
})

// Syntax: 
/*
app.httpMethod("/endpoint", (request, response) => {
	//code block
});
*/
app.put("/changepassword", (request, response) => {
    let message;
    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username){
            users[i].password = request.body.password
            message = `User ${request.body.username}'s password has been updated`
            break;
        } else {
            message = "User does not exist"
        }
    }
    console.log(users)
    response.send(message);
})


app.listen(port, () => console.log(`Server is running at port ${port}`));
// Tells our server/application to listen to the port
// If the port is accessed, we can run the server 
// 

// S34 Activity

app.get('/home', (req, res) => {
	res.send('Welcome to the homepage!')
})

app.get('/users', (req, res) => {
	res.send(users)
})

app.delete('/delete-user', (request, response) => {
	let message

	for( let i = 0; i < users.length; i++) {
		if(request.body.username == users[i].username && request.body.password == users[i].password){
			users.splice(i,1)
			message = `User ${request.body.username} has been deleted.`
			console.log(users)
			break
		} else {
			message = `Username does not exist.`
		}
	}
	response.send(message)
});

